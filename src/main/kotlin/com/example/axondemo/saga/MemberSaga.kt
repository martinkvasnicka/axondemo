package com.example.axondemo.saga

import com.example.axondemo.command.MatchMemberCommand
import com.example.axondemo.command.SetMarkrefCommand
import com.example.axondemo.domainmodel.MemberAggregateId
import com.example.axondemo.event.FirstTransferableConsentAddedEvent
import com.example.axondemo.event.LastTransferableConsentRemovedEvent
import com.example.axondemo.event.MarkrefAssignedEvent
import com.example.axondemo.event.MemberCreatedEvent
import com.example.axondemo.event.MemberDeletedEvent
import com.example.axondemo.event.MemberMatchedEvent
import com.example.axondemo.event.MembershipEndDateUpdatedEvent
import mu.KotlinLogging
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.modelling.saga.EndSaga
import org.axonframework.modelling.saga.SagaEventHandler
import org.axonframework.modelling.saga.StartSaga
import org.axonframework.spring.stereotype.Saga
import org.springframework.beans.factory.annotation.Autowired

@Saga
class MemberSaga {

    private val logger = KotlinLogging.logger {}

    @Autowired
    @Transient
    private lateinit var commandGateway: CommandGateway

    private val organizationMap: Map<String, String> = mapOf("orgA" to "201", "orgB" to "202")

    private var hasPartRef: Boolean = false
    private var hasMarkRef: Boolean = false
    private var hasTransferableConsent: Boolean = false

    @StartSaga
    @SagaEventHandler(associationProperty = "memberId")
    fun handle(event: MemberCreatedEvent) {
        logger.info("MemberSaga MemberCreatedEvent")
        commandGateway.send<Void>(MatchMemberCommand(event.memberId, false))
    }

    @StartSaga
    @SagaEventHandler(associationProperty = "memberId")
    fun handle(event: MembershipEndDateUpdatedEvent) {
        logger.info("MemberSaga MembershipEndDateUpdatedEvent")
        // how to check if we need to update GK?
    }

    @SagaEventHandler(associationProperty = "memberId")
    fun handle(event: FirstTransferableConsentAddedEvent) {
        logger.info("MemberSaga FirstTransferableConsentAddedEvent")
        hasTransferableConsent = true
        if (hasPartRef) {
            setMarkrefIfPossible(event.memberId)
        } else {
            commandGateway.send<Void>(MatchMemberCommand(event.memberId, true))
        }
    }

    @SagaEventHandler(associationProperty = "memberId")
    fun handle(event: MemberMatchedEvent) {
        logger.info("MemberSaga MemberMatchedEvent")
        hasPartRef = true
        setMarkrefIfPossible(event.memberId)
    }

    @SagaEventHandler(associationProperty = "memberId")
    fun handle(event: LastTransferableConsentRemovedEvent) {
        logger.info("LastTransferableConsentRemovedEvent")
        hasTransferableConsent = false
    }

    @EndSaga
    @SagaEventHandler(associationProperty = "memberId")
    fun handle(event: MemberDeletedEvent) {
        logger.info("MemberSaga MemberDeletedEvent")
    }

    @SagaEventHandler(associationProperty = "memberId")
    fun handle(event: MarkrefAssignedEvent) {
        logger.info("MemberSaga MarkrefAssignedEvent")
        this.hasMarkRef = true
    }

    private fun setMarkrefIfPossible(memberId: MemberAggregateId) {
        if (hasPartRef && hasTransferableConsent) {
            val markref = organizationMap[memberId.organizationCode]!!
            commandGateway.send<Void>(SetMarkrefCommand(memberId, "", markref))
        }
    }
}
