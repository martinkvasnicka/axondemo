package com.example.axondemo.integration.gk

import com.example.axondemo.domainmodel.MemberAggregateId
import com.example.axondemo.dummy.DummyGkImpl
import mu.KotlinLogging
import org.springframework.stereotype.Component

@Component
class GkIntegration(val gkImpl: DummyGkImpl) {

    private val logger = KotlinLogging.logger {}

    fun match(memberId: MemberAggregateId): String {
        logger.info("match")
        return gkImpl.match(memberId.externalId)
    }

    fun matchOrCreate(memberId: MemberAggregateId): String {
        logger.info("matchOrCreate")
        return gkImpl.matchOrCreate(memberId.externalId)
    }

    fun remove() {
        logger.info("remove")
    }

    fun setMarkref(memberId: MemberAggregateId, partref: String, markref: String) {
        logger.info("setMarkref")
        gkImpl.setMarkref(memberId.externalId, partref, markref)
    }
}
