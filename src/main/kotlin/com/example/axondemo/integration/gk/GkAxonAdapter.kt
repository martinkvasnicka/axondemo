package com.example.axondemo.integration.gk

import com.example.axondemo.command.AssignMarkrefCommand
import com.example.axondemo.command.AssignPartrefCommand
import com.example.axondemo.command.MatchMemberCommand
import com.example.axondemo.command.SetMarkrefCommand
import com.example.axondemo.event.FirstTransferableConsentAddedEvent
import com.example.axondemo.event.LastTransferableConsentRemovedEvent
import com.example.axondemo.event.MemberCreatedEvent
import mu.KotlinLogging
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.DisallowReplay
import org.axonframework.eventhandling.EventHandler
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils

@Component
//@ProcessingGroup("memberGroup")
class GkAxonAdapter(val gkIntegration: GkIntegration, val commandGateway: CommandGateway) {

    private val logger = KotlinLogging.logger {}

    @EventHandler
    @DisallowReplay
    fun on(event: MemberCreatedEvent) {
        logger.info("GkAxonAdapter MemberCreatedEvent")
    }

    @EventHandler
    @DisallowReplay
    fun on(event: FirstTransferableConsentAddedEvent) {
        logger.info("GkAxonAdapter FirstTransferableConsentAddedEvent")
    }

    @EventHandler
    fun on(event: LastTransferableConsentRemovedEvent) {
        logger.info("GkAxonAdapter LastTransferableConsentRemovedEvent")
        gkIntegration.remove()
    }

    @CommandHandler
    fun handle(cmd: MatchMemberCommand) {
        logger.info("GkAxonAdapter MatchMemberCommand")
        val partref =
            if (cmd.doCreate) {
                gkIntegration.matchOrCreate(cmd.memberId)
            } else {
                gkIntegration.match(cmd.memberId)
            }
        if (StringUtils.hasLength(partref)) {
            commandGateway.send<Any>(
                AssignPartrefCommand(cmd.memberId, partref)
            )
        }
    }

    @CommandHandler
    fun handle(cmd: SetMarkrefCommand) {
        logger.info("GkAxonAdapter SetMarkrefCommand")
        gkIntegration.setMarkref(cmd.memberId, cmd.partref, cmd.markref)
        commandGateway.send<Any>(
            AssignMarkrefCommand(cmd.memberId, cmd.partref, cmd.markref)
        )
    }
}
