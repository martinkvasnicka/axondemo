package com.example.axondemo.event

import com.example.axondemo.domainmodel.MemberAggregateId

data class MemberMatchedEvent(
    override val memberId: MemberAggregateId,
    val partref: String
) : ParentMemberEvent(memberId)
