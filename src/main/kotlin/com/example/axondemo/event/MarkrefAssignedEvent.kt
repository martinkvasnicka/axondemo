package com.example.axondemo.event

import com.example.axondemo.domainmodel.MemberAggregateId

data class MarkrefAssignedEvent(
    override val memberId: MemberAggregateId,
    val partref: String,
    val markref: String,
) : ParentMemberEvent(memberId)
