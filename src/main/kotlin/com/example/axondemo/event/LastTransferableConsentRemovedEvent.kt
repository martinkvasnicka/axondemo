package com.example.axondemo.event

import com.example.axondemo.domainmodel.MemberAggregateId

data class LastTransferableConsentRemovedEvent(override val memberId: MemberAggregateId) :
    ParentMemberEvent(memberId)
