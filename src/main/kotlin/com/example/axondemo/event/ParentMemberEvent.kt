package com.example.axondemo.event

import com.example.axondemo.domainmodel.MemberAggregateId

abstract class ParentMemberEvent(
    open val memberId: MemberAggregateId
)
