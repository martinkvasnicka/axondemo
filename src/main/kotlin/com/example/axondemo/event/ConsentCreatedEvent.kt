package com.example.axondemo.event

import com.example.axondemo.domainmodel.MemberAggregateId
import java.time.LocalDate

data class ConsentCreatedEvent(
    override val memberId: MemberAggregateId,
    val consentCode: String,
    val validFrom: LocalDate?,
    val validTo: LocalDate?
) : ParentMemberEvent(memberId)
