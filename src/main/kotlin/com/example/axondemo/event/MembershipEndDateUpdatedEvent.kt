package com.example.axondemo.event

import com.example.axondemo.domainmodel.MemberAggregateId
import java.time.LocalDate

data class MembershipEndDateUpdatedEvent(override val memberId: MemberAggregateId, val date: LocalDate?) :
    ParentMemberEvent(memberId)
