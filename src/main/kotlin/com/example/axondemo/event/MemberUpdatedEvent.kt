package com.example.axondemo.event

import com.example.axondemo.domainmodel.MemberAggregateId
import com.example.axondemo.model.MemberData
import java.time.LocalDate

data class MemberUpdatedEvent(
    override val memberId: MemberAggregateId,
    private val firstName: String,
    private val membershipEndDate: LocalDate
) : ParentMemberEvent(memberId), MemberData {
    override fun getFirstName() = firstName

    override fun getMembershipEndDate() = membershipEndDate
}
