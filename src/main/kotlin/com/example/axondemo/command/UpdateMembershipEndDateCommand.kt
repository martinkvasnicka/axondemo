package com.example.axondemo.command

import com.example.axondemo.domainmodel.MemberAggregateId
import java.time.LocalDate

data class UpdateMembershipEndDateCommand(
    override val memberId: MemberAggregateId,

    val date: LocalDate?
) : MemberCommand(memberId)
