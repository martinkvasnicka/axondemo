package com.example.axondemo.command

import com.example.axondemo.domainmodel.MemberAggregateId
import java.time.LocalDate

data class CreateConsentCommand(
    override val memberId: MemberAggregateId,

    val consentCode: String,
    val validFrom: LocalDate?,
    val validTo: LocalDate?
) : MemberCommand(memberId)
