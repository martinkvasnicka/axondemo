package com.example.axondemo.command

import com.example.axondemo.domainmodel.MemberAggregateId

data class AssignPartrefCommand(
    override val memberId: MemberAggregateId,
    val partref: String
) : MemberCommand(memberId)
