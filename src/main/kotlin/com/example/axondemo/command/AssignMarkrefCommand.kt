package com.example.axondemo.command

import com.example.axondemo.domainmodel.MemberAggregateId

data class AssignMarkrefCommand(
    override val memberId: MemberAggregateId,
    val partref: String,
    val markref: String
) : MemberCommand(memberId)
