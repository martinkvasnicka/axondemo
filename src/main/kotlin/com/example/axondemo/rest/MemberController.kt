package com.example.axondemo.rest

import com.example.axondemo.command.CreateConsentCommand
import com.example.axondemo.command.CreateMemberCommand
import com.example.axondemo.command.DeleteMemberCommand
import com.example.axondemo.command.UpdateConsentCommand
import com.example.axondemo.command.UpdateMemberCommand
import com.example.axondemo.command.UpdateMembershipEndDateCommand
import com.example.axondemo.domainmodel.MemberAggregateId
import com.example.axondemo.model.ConsentRequest
import com.example.axondemo.model.Member
import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate

@RestController
@RequestMapping("/org/{organizationCode}/member/{externalId}")
class MemberController(val commandGateway: CommandGateway) {

    @PostMapping("")
    fun postPerson(
        @PathVariable organizationCode: String,
        @PathVariable externalId: String,
        @RequestBody member: Member
    ): ResponseEntity<Nothing> {
        // returns aggregateId
        commandGateway.send<String>(
            CreateMemberCommand(MemberAggregateId(organizationCode, externalId), member.firstName, member.membershipEndDate)
        )
        return ResponseEntity.status(HttpStatus.CREATED).build()
    }

    @PutMapping("")
    fun putPerson(
        @PathVariable organizationCode: String,
        @PathVariable externalId: String,
        @RequestBody member: Member
    ): ResponseEntity<Nothing> {
        commandGateway.send<Void>(
            UpdateMemberCommand(MemberAggregateId(organizationCode, externalId), member.firstName, member.membershipEndDate)
        )
        return ResponseEntity.status(HttpStatus.OK).build()
    }

    @DeleteMapping("")
    fun deletePerson(
        @PathVariable organizationCode: String,
        @PathVariable externalId: String
    ): ResponseEntity<Nothing> {
        commandGateway.send<Void>(
            DeleteMemberCommand(MemberAggregateId(organizationCode, externalId))
        )
        return ResponseEntity.status(HttpStatus.OK).build()
    }

    @PutMapping("/membershipenddate")
    fun updateMembershipEndDate(
        @PathVariable organizationCode: String,
        @PathVariable externalId: String,
        @RequestBody(required = false) endDate: LocalDate?
    ): ResponseEntity<Nothing> {
        commandGateway.send<Void>(
            UpdateMembershipEndDateCommand(MemberAggregateId(organizationCode, externalId), endDate)
        )
        return ResponseEntity.status(HttpStatus.OK).build()
    }

    @PostMapping("/consents/{consentCode}")
    fun postPersonConsent(
        @PathVariable organizationCode: String,
        @PathVariable externalId: String,
        @PathVariable consentCode: String,
        @RequestBody consentRequest: ConsentRequest
    ): ResponseEntity<Nothing> {
        commandGateway.send<Void>(
            CreateConsentCommand(
                MemberAggregateId(organizationCode, externalId),
                consentCode, consentRequest.validFrom, consentRequest.validTo
            )
        )
        return ResponseEntity.status(HttpStatus.OK).build()
    }

    @PutMapping("/consents/{consentCode}")
    fun putPersonConsent(
        @PathVariable organizationCode: String,
        @PathVariable externalId: String,
        @PathVariable consentCode: String,
        @RequestBody consentRequest: ConsentRequest
    ): ResponseEntity<Nothing> {
        commandGateway.send<Void>(
            UpdateConsentCommand(
                MemberAggregateId(organizationCode, externalId),
                consentCode, consentRequest.validFrom, consentRequest.validTo
            )
        )
        return ResponseEntity.status(HttpStatus.OK).build()
    }
}
