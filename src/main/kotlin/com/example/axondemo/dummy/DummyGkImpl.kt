package com.example.axondemo.dummy

import org.springframework.stereotype.Component

@Component
class DummyGkImpl {

    fun match(externalId: String): String {
        Thread.sleep(2000)
        return if (externalId == "1") {
            return "101"
        } else {
            ""
        }
    }

    fun matchOrCreate(externalId: String): String {
        Thread.sleep(1000)
        return if (externalId == "2") {
            return "102"
        } else {
            ""
        }
    }

    fun setMarkref(externalId: String, partref: String, markref: String) {
        // nothing
    }
}
