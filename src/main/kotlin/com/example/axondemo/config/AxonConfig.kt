package com.example.axondemo.config

import org.axonframework.commandhandling.AsynchronousCommandBus
import org.axonframework.common.caching.Cache
import org.axonframework.common.caching.WeakReferenceCache
import org.axonframework.common.jdbc.ConnectionProvider
import org.axonframework.common.transaction.TransactionManager
import org.axonframework.config.EventProcessingConfigurer
import org.axonframework.config.EventProcessingModule
import org.axonframework.eventhandling.PropagatingErrorHandler
import org.axonframework.eventhandling.async.SequentialPerAggregatePolicy
import org.axonframework.eventhandling.tokenstore.jdbc.GenericTokenTableFactory
import org.axonframework.eventhandling.tokenstore.jdbc.JdbcTokenStore
import org.axonframework.eventsourcing.EventCountSnapshotTriggerDefinition
import org.axonframework.eventsourcing.SnapshotTriggerDefinition
import org.axonframework.eventsourcing.eventstore.jdbc.HsqlEventTableFactory
import org.axonframework.eventsourcing.eventstore.jdbc.JdbcEventStorageEngine
import org.axonframework.messaging.interceptors.CorrelationDataInterceptor
import org.axonframework.modelling.saga.repository.jdbc.HsqlSagaSqlSchema
import org.axonframework.modelling.saga.repository.jdbc.JdbcSagaStore
import org.axonframework.serialization.Serializer
import org.axonframework.spring.config.AxonConfiguration
import org.axonframework.spring.eventsourcing.SpringAggregateSnapshotter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class AxonConfig {
//    @EventListener
//    fun handleContextRefresh(event: ContextRefreshedEvent) {
//        configureCommandBus(event.applicationContext.getBean(SimpleCommandBus::class.java))
//    }
//
//    private fun configureCommandBus(commandBus: SimpleCommandBus) {
//        commandBus.setRollbackConfiguration(RollbackConfigurationType.ANY_THROWABLE)
//    }

    @Bean
    fun eventProcessingModule(): EventProcessingModule {
        val eventProcessingModule = EventProcessingModule()
        eventProcessingModule.registerDefaultListenerInvocationErrorHandler { PropagatingErrorHandler.INSTANCE }
        return eventProcessingModule
    }

    @Bean
    fun aggregateCache(): Cache {
        return WeakReferenceCache()
    }

    @Bean
    fun snapshotTriggerDefinition(snapshotter: SpringAggregateSnapshotter): SnapshotTriggerDefinition {
        return EventCountSnapshotTriggerDefinition(snapshotter, 5)
    }

    // The Event store `EmbeddedEventStore` delegates actual storage and retrieval of events to an `EventStorageEngine`.
//    @Bean
//    fun eventStore(storageEngine: EventStorageEngine, configuration: AxonConfiguration): EmbeddedEventStore {
//        return EmbeddedEventStore.builder()
//            .storageEngine(storageEngine)
//            .messageMonitor(configuration.messageMonitor(EventStore::class.java, "eventStore"))
//            .build()
//    }

    @Bean
    fun commandBus(txManager: TransactionManager, configuration: AxonConfiguration): AsynchronousCommandBus {
        val commandBus = AsynchronousCommandBus
            .builder()
            .transactionManager(txManager)
            .messageMonitor(
                configuration.messageMonitor(
                    AsynchronousCommandBus::class.java,
                    "commandBus"
                )
            )
            .build()
        commandBus.registerHandlerInterceptor(CorrelationDataInterceptor(configuration.correlationDataProviders()))
        return commandBus
    }

    @Autowired
    fun configure(configurer: EventProcessingConfigurer) {
        configurer
            .usingTrackingEventProcessors()
            .registerDefaultSequencingPolicy { _ -> SequentialPerAggregatePolicy.instance() }
    }

//    @Bean
//    fun mySagaConfiguration(): SagaConfiguration<Saga>? {
//        return SagaConfiguration.trackingSagaManager(MemberSaga::class.java)
//    }

    // The `InMemoryEventStorageEngine` stores each event in memory
//    @Bean
//    fun storageEngine(): EventStorageEngine {
//        return InMemoryEventStorageEngine()
//    }

    @Bean
    fun jdbcEventStore(connectionProvider: ConnectionProvider, transactionManager: TransactionManager): JdbcEventStorageEngine {
        val result = JdbcEventStorageEngine.builder()
            .connectionProvider(connectionProvider)
            .transactionManager(transactionManager)
            .build()
        result.createSchema(HsqlEventTableFactory.INSTANCE)

        return result
    }

    @Bean
    fun jdbcTokenStore(connectionProvider: ConnectionProvider, serializer: Serializer): JdbcTokenStore {
        val result = JdbcTokenStore.builder()
            .connectionProvider(connectionProvider)
            .serializer(serializer)
            .build()
        result.createSchema(GenericTokenTableFactory.INSTANCE)

        return result
    }

    @Bean
    fun jdbcSagaStore(connectionProvider: ConnectionProvider, transactionManager: TransactionManager): JdbcSagaStore {
        val result = JdbcSagaStore.builder()
            .connectionProvider(connectionProvider)
            .sqlSchema(HsqlSagaSqlSchema())
            .build()
//        result.createSchema()

        return result
    }
}
