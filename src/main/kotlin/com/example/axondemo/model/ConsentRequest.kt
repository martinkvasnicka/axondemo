package com.example.axondemo.model

import java.time.LocalDate

data class ConsentRequest(val validFrom: LocalDate?, val validTo: LocalDate?)