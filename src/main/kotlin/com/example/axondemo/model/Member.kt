package com.example.axondemo.model

import java.time.LocalDate

data class Member(
    val firstName: String,
    val membershipEndDate: LocalDate
)
