package com.example.axondemo.model

import java.time.LocalDate

interface MemberData {

    fun getFirstName(): String
    fun getMembershipEndDate(): LocalDate
}
