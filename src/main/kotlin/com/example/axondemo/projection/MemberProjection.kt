package com.example.axondemo.projection

import com.example.axondemo.domainmodel.MemberAggregate
import com.example.axondemo.event.ParentMemberEvent
import mu.KotlinLogging
import org.axonframework.eventhandling.EventHandler
import org.axonframework.modelling.command.Repository
import org.springframework.stereotype.Component

@Component
class MemberProjection {

    private val logger = KotlinLogging.logger {}

    @EventHandler
    fun handleAnyEvent(event: ParentMemberEvent, memberRepository: Repository<MemberAggregate>) {
        logger.info("projecting ${event.javaClass}")
        memberRepository.load(event.memberId.toString()).execute {
            println(it)
        }
    }
}
